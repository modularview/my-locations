import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from './category.service';
import { Category } from './category';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

    @ViewChild('categoryField') categoryField: ElementRef;

    categories: Array<string>;
    showAddPanel = false;
    categoryName: string;
    submitError: string;
    newItem: string;

    constructor(private categoryService: CategoryService) {
    }


    ngOnInit() {
        this.updateListView();
    }


    updateListView(){
        const cat = this.categoryService.categories;
        this.categories = cat.list;
    }


    toggleAddCategory(show: boolean) {
        this.showAddPanel = show;
        setTimeout(() => { this.categoryField.nativeElement.focus(); }, 200);
    }


    addCategory() {
        try {
            this.categoryService.addCategory(this.categoryName);
        } catch (e) {
            this.submitError = e;
            return;
        }

        this.submitError = null;
        this.showAddPanel = false;

        this.updateListView();
        this.newItem = this.categoryName;
        this.categoryName = null;
    }


    deleteCategory(name: string) {
        this.categoryService.deleteCategory(name);
        this.updateListView();
    }


    editCategory(data: Category) {
        this.categoryService.editCategory(data.origValue, data.value);
        this.updateListView();
    }

}
