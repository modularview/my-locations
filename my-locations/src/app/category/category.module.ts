import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CategoryComponent } from './category.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        CategoryComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule
    ]
})
export class CategoryModule {
}
