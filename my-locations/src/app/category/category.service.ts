import { Injectable } from '@angular/core';
import { Categories } from './category';

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    categories: Categories;
    private readonly categoryStorageKey = 'mlCategories';

    constructor() {
        this.categories = new Categories();
        this.updateCategories();
    }


    updateCategories() {
        const categoryStore = JSON.parse(localStorage.getItem(this.categoryStorageKey));
        if (categoryStore) {
            this.categories.list = categoryStore;
        }
    }


    addCategory(categoryName: string) {
        this.updateCategories();
        this.categories.addItem(categoryName);
        this.categories.list.sort((a, b) => a < b ? -1 : a > b ? 1 : 0);
        localStorage.setItem(this.categoryStorageKey, JSON.stringify(this.categories.list));
    }


    editCategory(oldName: string, newName: string) {
        this.categories.updateCategory(oldName, newName);
        localStorage.setItem(this.categoryStorageKey, JSON.stringify(this.categories.list));
        this.updateCategories();
    }


    deleteCategory(categoryName: string) {
        this.categories.removeItem(categoryName);
        localStorage.setItem(this.categoryStorageKey, JSON.stringify(this.categories.list));
        this.updateCategories();
    }
}
