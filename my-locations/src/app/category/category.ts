export interface Category {
    origValue: string;
    value: string;
}

// TODO
// export class Category {
//
// }

export class Categories {

    private categoriesList: Array<string>;

    set list(items: Array<string>)  {
        this.categoriesList = items;
    }

    get list(): Array<string> {
        return this.categoriesList;
    }

    constructor() {
        this.categoriesList = [];
    }


    addItem(categoryName: string) {
        if (!categoryName || categoryName.length > 30) {
            throw new Error('invalid category name, too long or undefined');
        }

        const index = this.categoriesList.indexOf(categoryName);
        if (index > -1){
            throw new Error('category name must be unique');
        }

        this.categoriesList.push(categoryName);
    }


    removeItem(categoryName: string) {
        const index = this.categoriesList.indexOf(categoryName);
        if (index === -1) {
            throw new Error('category name does not exists');
        }
        this.categoriesList.splice(index, 1);
    }


    updateCategory(oldName: string, newName: string) {
        const index = this.categoriesList.indexOf(oldName);
        if (index === -1) {
            throw new Error('category name does not exists');
        }
        this.categoriesList.splice(index, 1, newName);
    }

}
