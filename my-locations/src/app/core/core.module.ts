import { ErrorHandler, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalErrorHandlerService } from './services/global-error-handler.service';
import { CategoryService } from '../category/category.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    providers: [
        CategoryService,
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandlerService
        }
    ]
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}
