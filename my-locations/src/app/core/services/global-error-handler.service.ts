import {ErrorHandler, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';


@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {
    handleError(error) {
        const message = error.message ? error.message : error.toString();
        if (!environment.production) {
            console.error(message);
        }
    }
}
