import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationComponent } from './location.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        LocationComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ]
})
export class LocationModule {
}
