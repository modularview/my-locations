import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocationService {

    constructor() {
    }


    viewLocationsList(){}
    addLocation(){}
    editLocation(){}
    deleteLocation(){}
}
