export class Location {

    private locationName: string;
    private locationAddress: string;
    private locationCoordinates: string;
    private locationCategory: string;

    get name(): string {
        return this.locationName;
    }


    set name(v: string) {
        this.locationName = v;
    }


    get address(): string {
        return this.locationAddress;
    }


    set address(v: string) {
        this.locationAddress = v;
    }


    get coordinates(): string {
        return this.locationCoordinates;
    }


    set coordinates(v: string) {
        this.locationCoordinates = v;
    }


    get category(): string {
        return this.locationCategory;
    }


    set category(v: string) {
        this.locationCategory = v;
    }


    isValid(): boolean {
        if (!this.name || this.name.length > 30) {
            return false;
        }
        if (!this.address || !this.coordinates || !this.category) {
            return false;
        }
        return true;
    }
}
