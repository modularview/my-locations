import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Category } from '../../category/category';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent {
    private readonly hideCss = 'd-none';
    private readonly itemIdPrefix = 'list-item';
    private readonly fieldIdPrefix = 'list-field';
    private readonly activeCss = 'active';

    @Input() editMode = false;
    @Input() category: string; // TODO : location type
    @Input() itemIndex: number;
    @Input() latestName: string;
    @Output() editClick = new EventEmitter<Category>();
    @Output() deleteClick = new EventEmitter<string>();
    @ViewChild('categoryInput') categoryInput: ElementRef;

    private categoryOrig: string;


    itemOver(event) {
        event.target.classList.add(this.activeCss);
    }


    itemOut(event) {
        event.target.classList.remove(this.activeCss);
    }


    onEditClick() {
        if (this.categoryOrig === this.category) {
            return;
        }
        this.editClick.emit({origValue: this.categoryOrig, value: this.category});
    }


    onDeleteClick(item: string) {
        this.deleteClick.emit(item);
    }


    toggleEditField(show) {
        const itemText = document.getElementById(this.itemIdPrefix + this.itemIndex);
        const editField = document.getElementById(this.fieldIdPrefix + this.itemIndex);

        if (show) {
            this.categoryOrig = this.category;
            itemText.classList.add(this.hideCss);
            editField.classList.remove(this.hideCss);
            setTimeout(() => { this.categoryInput.nativeElement.focus(); }, 200);
        } else {
            itemText.classList.remove(this.hideCss);
            editField.classList.add(this.hideCss);
        }
    }


    attachActions(event: KeyboardEvent){
        if (event.key === 'Escape') {
            this.toggleEditField(false);
        }
        else if (event.key === 'Enter') {
            this.onEditClick();
        }
    }

}
