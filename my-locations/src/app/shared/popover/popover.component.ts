import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-popover',
    templateUrl: './popover.component.html',
    styleUrls: ['./popover.component.scss']
})
export class PopoverComponent implements OnChanges {

    @Input() title: string;
    @Input() show: boolean;
    @Output() buttonClick = new EventEmitter();
    visibilityCss: string;

    ngOnChanges(changes: SimpleChanges): void {
        this.visibilityCss = (this.show === true) ? 'ml-popup-expand' : '';
    }

    onButtonClick() {
        this.buttonClick.emit();
    }

}
