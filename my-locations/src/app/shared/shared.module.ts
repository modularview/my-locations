import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TopbarComponent } from './topbar/topbar.component';
import { PopoverComponent } from './popover/popover.component';
import { ListComponent } from './list/list.component';

@NgModule({
    declarations: [
        TopbarComponent,
        PopoverComponent,
        ListComponent
    ],
    exports: [
        TopbarComponent,
        PopoverComponent,
        ListComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ]
})
export class SharedModule {
}
