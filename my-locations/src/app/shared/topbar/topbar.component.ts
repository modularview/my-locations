import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {

    @Input() title: string;
    @Output() buttonClick = new EventEmitter();


    onButtonClick() {
        this.buttonClick.emit();
    }

}
